# autostart_scripts
### Making installation automation on SteamOS easy
autostart_scripts is a collection of Python tools to make scripting an automatic installation on SteamOS extremely easy. An example autostart script is already provided with my favorite utilities. The provided `install.sh` script will configure everything for you. You just need to run it, restart your Steam Deck, and when you return to Desktop Mode, it should already start prompting you to install missing packages if you haven't installed them.

## Installing
Installing autostart_scripts is really easy. You can simply run this command:
```
bash -c "$(curl -fsSL https://gitlab.com/nickgirga/autostart_scripts/-/raw/main/tools/install.sh)"
```
This will pipe the output that curl gets from the remote script into bash. The install script will simply clone the repository into "\~/.local/share/autostart_scripts" and create a symbolic link from the desktop file in the cloned repository to "\~/.config/autostart/Auto Installer.desktop" so that it will actually start whenever you enter Desktop Mode.

After you run that command, everything is configured. If you like my settings, simply restart your Deck. You're done. If you want to configure which packages autostart_scripts will look for and install, continue to the [configuring](#configuring) section.

## Uninstalling
Uninstalling autostart_scripts is really easy as well. You can just run the uninstall script located at `~/.local/share/autostart_scripts/tools/uninstall.sh`. This will remove the repository that was cloned to "\~/.local/share/autostart_scripts/" and it will also remove the symbolic link created at "\~/.config/autostart/Auto Installer.desktop". Any packages installed using autostart_scripts and any custom desktop files created in "\~/.config/autostart" will remain. You must manually remove these if you wish.

## Configuring
To configure which packages autostart_scripts installs, it's really simple: you just need to modify the script located at `~/.local/share/autostart_scripts/my_autostart_installer.py`. This will give you a good idea of how the library's interface works. If you feel comfortable with it, you can create separate configurations to easily "enable" and "disable" groups of packages (e.g. development packages you know you may not use for months at a time). You will need to manually create new desktop files for these to place in the user's autostart folder, but the existing one should serve as an easy template.

As you can see, the fundamental ideas behind autostart_scripts are fairly simple: check for evidence of the package (e.g. is the command available or does a library file exist), then install it if it is not found. As a user, you have a few useful functions to utilize in order to achieve what you want: [`command_exists(...)`](#command_exists), [`file_exists(...)`](#file_exists), [`pacman_install(...)`](#pacman_install), [`drop_sudo()`](#drop_sudo), and [`create_zenity_dialog(...)`](#create_zenity_dialog)

**Note: using an IDE with code completion and comment scraping will help you understand as you write your code**

<br />

### command_exists(...)
`command_exists(command: str) -> bool` is useful for determining if a command is available on a machine. For example, I can use it to determine if `gocryptfs` is installed by asking if `command_exists("gocryptfs")`.

It accepts the command alone as a string for an argument. A `True` response means that the command does indeed exist on the system, so installing it would probably not be necessary.

<br />

### file_exists(...)
`file_exists(potential_paths: List[str]) -> bool` is useful for determining if any one file exists on a machine. For example, I can use it to determine if `ffmpegthumbs` is installed by asking if `file_exists(["/usr/lib/qt/plugins/ffmpegthumbs.so"])`.

It accepts a list of strings that represent possible paths for the file you are looking for as an argument. A `True` response means that one (or more) of the potential files does indeed exist on the system, so installing it would probably not be necessary.

<br />

### pacman_install(...)
`pacman_install(packages: List[str], additional_parameters: List[str] = None, prompt = True, completion_notif = True, overwrite_conflicts = False, drop_sudo_when_done = True)` is the command that you will use to install packages to the system. For example, to install Ardour, I can simply use `pacman_install(["ardour"])`. This will automatically disable SteamOS's read-only protection, install your software, and re-enable the read-only protection. It will also initialize pacman's keyring if needed. You should not need to worry about anything but running this command. In theory, you could use this command alone to simply ask pacman to install every package you might want (as the default configuration will skip installed packages). This is just not very efficient, as it still relies on querying a server (which is why we only use pacman if we need to).

It accepts a list of strings that represent packages you wish to install for the first argument. You can then specify any additional parameters that you may want to pass to pacman using the `additional_parameters` argument by passing another list of strings. The `prompt` argument is set to `True` by default and will ask the user if they would like to install the software before continuing. The `completion_notif` argument is set to `True` by default and will notify the user when the installation is complete. `overwrite_conflicts` is set to `False` by default because it is a destructive option, but it may be needed for some applications to properly reinstall after an update on SteamOS (for example, I had a residual KDE Connect desktop file bork an install for me one time). This will force pacman to overwrite any foreign files that it runs into during install. Try to avoid using the `overwrite_conflicts` option if you use other package managers as well (e.g. `pip`, `brew`), as it may break them if you overwrite files/packages managed by them. `drop_sudo_when_done` is set to `True` by default in order to prevent abuse of the sudo timeout. If you wish to install software in stages and do not want to have multiple sudo prompts, you can set the option to `False`, so that sudo privileges will remain active after the install completes. Remember to run `drop_sudo()` after you have completed everything if you decide to set the `drop_sudo_when_done` option to `False`. Nothing catastrophic will happen, but it just isn't very secure to leave sudo with its timeout active (as anyone passing by could run a privileged command without typing your password in until the timeout expires).

<br />

### drop_sudo()
Incredibly simple. Drops privileges. This shouldn't need to be used unless you explicitly disable the automatic privilege dropping that the `pacman_install(...)` command already utilizes. After running `drop_sudo()`, you will need to enter your user password again if sudo is needed.

<br />

### create_zenity_dialog(...)
`create_zenity_dialog(message: str, dialog_type: ZenityDialogType = ZenityDialogType.INFO) -> bool` is incredibly useful for easily creating dialog windows for the user. It accepts the message as a string for the first argument and a ZenityDialogType for the second in order to define what kind of window should show. ZenityDialogType is just an Enum I created to represent the different zenity dialog windows that I've implemented in Python with the command line interface so far. It consists of three so far:
 - `ZenityDialogType.INFO`
 - `ZenityDialogType.ERROR`
 - `ZenityDialogType.QUESTION`

When the dialog is closed, it will only return `False` if a "No" button is pressed. So, pressing "Yes" in the question dialog *or* pressing "OK" will result in a response of `True`.

<br />

### Putting it Together
Here is an extremely basic example to keep `cowsay` installed:
```
from autostart_installers import *

if not command_exists("cowsay"):
	pacman_install(["cowsay"])
```

<br />

Here's a bit more complicated of an example to keep `gocryptfs`, `ffmpegthumbs`, and `kdeconnect` installed:
```
from autostart_installers import *

missing_packages = [] # create empty package list

if not command_exists("gocryptfs"):
	missing_packages += ["gocryptfs"] # add to package list

if not file_exists(["/usr/lib/qt/plugins/ffmpegthumbs.so"]):
	missing_packages += ["ffmpegthumbs"] # add to package list

if not file_exists(["/usr/lib/kdeconnectd"]):
	# install kdeconnect with overwrite_conflicts enabled; don't drop sudo
	pacman_install(["kdeconnect"], overwrite_conflicts=True, drop_sudo_when_done=False)
 
	# start KDE Connect daemon
	try:
		pid = os.fork() # create independent process for daemon
		if not pid:
			subprocess.run(["/usr/lib/kdeconnectd"], capture_output=True)
			exit()
	except Exception as e:
	error_msg = str(e) + "\nWARNING!: An exception occurred while running `/usr/lib/kdeconnectd`!"
	print(error_msg, ' ', '\n', sys.stderr)
	create_zenity_dialog(error_msg, ZenityDialogType.ERROR)

if missing_packages != []:
	pacman_install(missing_packages) # install populated packages list

drop_sudo() # drop sudo privileges to prevent abuse of sudo timeout
```

Notice how we can use a package list to figure out which packages we're going to install before prompting the user. This way, we can avoid asking them to install each package individually.

You may have also noticed that we utilized an additional `pacman_install(...)` call in order to install KDE Connect in a reliable way (by overwriting potential residual files). By not dropping sudo privileges, we can continue installing the rest of the packages without disturbing the user for an action that has already essentially been confirmed.

You may have also noticed that we initialized KDE Connect a bit after its install. One of the advantages to using a scripting language as the basis for the instructions that we give our "auto installation system" is that we can use it to perform other tasks as well. Here, we simply create a separate process (to prevent it from dying when the script ends) and run the KDE Connect daemon as a subprocess to that new process. We don't need this subprocess to do anything else and we don't want it to continue running an additional install alongside the original one, so simply exit after kdeconnect presumably returns. We could have also changed configuration files, curled themes to a system directory, etc.

## Help Me Define a Package
If you're having trouble defining a package for installation, the most useful command you can use on SteamOS would probably be `pacman -Ql <installed_package>`. This will list all of the files provided by the specified "installed_package". Similarly, you can use `pacman -Qo <file_path|command>` to find which installed package owns the specified file at "file_path" or "command". Another useful command is the `which <command>` command, which will tell you the file path of the specified command in the current environment and is more distro-agnostic than the pacman-based commands. The final useful command I've leave you with is `grep [options] <pattern>`, which will filter text and make it easier to spot your pattern. Common usage is `grep -i <pattern>`, as this will remove case sensitivity.

For example, if I really wanted to find which package provided the `telnet` command, I can simply say `pacman -Qo telnet` and it will tell me that it is provided by a package named `inetutils`.

For an example in the opposite direction, if I really wanted to find a file to determine if `ffmpegthumbs` is installed, I could use `pacman -Ql ffmpegthumbs` to show all of the files that the package owns and I could pipe its output through grep to filter out the useless results: `pacman -Ql ffmpegthumbs | grep -i "\.so"`. We used `"\.so"` as a filter pattern because we're expecting to find a library (and Linux libraries are typically compiled to ".so" files). We must surround it with quotation marks and escape the period symbol because a period represents any character in regex. Doing this will mean that our period is actually interpreted as a period. Obviously, you can try different filter patters to help locate different kinds of files that may be useful to monitor for the absence of a package.

That's about all the help I can offer for you to configure your autostart_scripts. The rest is up to you, your Python capabilities, and your creativity.
