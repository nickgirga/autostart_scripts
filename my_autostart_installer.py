#!/usr/bin/env python
# my_autostart_installer.py
# by Nicholas Girga

# Nick's personal configuration for his Steam Deck

# Released under the MIT license with ABSOLUTELY NO WARRANTY


# repository for the Wallpaper Engine KDE Plasma plugin
WALLPAPER_ENGINE_PLUGIN_REPO = "git@github.com:catsout/wallpaper-engine-kde-plugin.git"
WALLPAPER_ENGINE_PLUGIN_RELEASE_TAG = "v0.5.4"


# import everything from library
from autostart_installers import *

# import recursive directory removal
from shutil import rmtree

# import cpu_count
from multiprocessing import cpu_count


# build and install the Wallpaper Engine KDE Plasma plugin; returns False if skipped
def build_wallpaper_engine_kde_plugin(drop_sudo_when_done = True) -> bool:
    if not False: # todo: implement
        if not create_zenity_dialog("Would you like to build and install the Wallpaper Engine KDE Plugin?", ZenityDialogType.QUESTION):
            return False

        # define temporary repo directory
        repo_dir = "/tmp/wallpaper-engine-kde-plugin"
        build_dir = repo_dir + "/build"

        if os.path.isdir(repo_dir):
            if create_zenity_dialog("A build directory for Wallpaper Engine KDE Plugin already exists at \"" + repo_dir + "\". Overwrite it?", ZenityDialogType.QUESTION):
                try:
                    rmtree(repo_dir)
                except Exception as e:
                    error_msg = str(e) + "\nERROR!: An exception occurred while removing the old Wallpaper Engine KDE Plugin repository!"
                    print(error_msg, ' ', '\n', sys.stderr)
                    create_zenity_dialog(error_msg, ZenityDialogType.ERROR)
                    return False
            else:
                return False

        # disable read-only protection
        if steamos_readonly_status():
            steamos_readonly_disable()

        # install build dependencies
        if not pacman_install(["extra-cmake-modules", "plasma-framework", "gst-libav", "base-devel", "mpv", "python-websockets", "qt5-base", "qt5-declarative", "qt5-websockets", "qt5-webchannel", "vulkan-headers", "cmake"], needed=False, overwrite_conflicts=True, drop_sudo_when_done=False):
            return False

        # clone repo and submodules
        msg = "Cloning repository..."
        print(msg)
        create_zenity_dialog(msg)
        try:
            subprocess.run(["git", "clone", "--recurse-submodules", "-b", WALLPAPER_ENGINE_PLUGIN_RELEASE_TAG, WALLPAPER_ENGINE_PLUGIN_REPO, repo_dir])
        except Exception as e:
            error_msg = str(e) + "\nERROR!: An exception occurred while trying to clone the Wallpaper Engine KDE Plugin repository!"
            print(error_msg, ' ', '\n', sys.stderr)
            create_zenity_dialog(error_msg, ZenityDialogType.ERROR)
            return False
        msg = "Finished cloning repository!"
        print(msg)
        create_zenity_dialog(msg)

        # create build directory and cd into it
        os.mkdir(build_dir)
        os.chdir(build_dir)

        # configure build
        msg = "Configuring build..."
        print(msg)
        create_zenity_dialog(msg)
        try:
            subprocess.run(["cmake", "..", "-DUSE_PLASMAPKG=ON"])
        except Exception as e:
            error_msg = str(e) + "\nERROR!: An exception occurred while running cmake!"
            print(error_msg, ' ', '\n', sys.stderr)
            create_zenity_dialog(error_msg, ZenityDialogType.ERROR)
            return False
        msg = "Finished configuring build!"
        print(msg)
        create_zenity_dialog(msg)

        # build
        msg = "Building..."
        print(msg)
        create_zenity_dialog(msg)
        try:
            subprocess.run(["make", "-j" + cpu_count()])
        except Exception as e:
            error_msg = str(e) + "\nERROR!: An exception occurred while building!"
            print(error_msg, ' ', '\n', sys.stderr)
            create_zenity_dialog(error_msg, ZenityDialogType.ERROR)
            return False
        msg = "Finished building!"
        print(msg)
        create_zenity_dialog(msg)

        # install package
        msg = "Installing..."
        print(msg)
        create_zenity_dialog(msg)
        try:
            subprocess.run(["make", "install_pkg"])
        except Exception as e:
            error_msg = str(e) + "\nERROR!: An exception occurred while installing package!"
            print(error_msg, ' ', '\n', sys.stderr)
            create_zenity_dialog(error_msg, ZenityDialogType.ERROR)
            return False

        # install lib
        try:
            subprocess.run(["sudo", "-A", "make", "install"], host_env)
        except Exception as e:
            error_msg = str(e) + "\nERROR!: An exception occurred while installing library!"
            print(error_msg, ' ', '\n', sys.stderr)
            create_zenity_dialog(error_msg, ZenityDialogType.ERROR)
            return False

        # enable read-only protection
        if not steamos_readonly_status():
            steamos_readonly_enable()

        # drop sudo privileges
        if drop_sudo_when_done:
            drop_sudo()

        # inform completion
        msg = "Finished installing Wallpaper Engine KDE Plugin!"
        print(msg)
        create_zenity_dialog(msg)
        
        return True


# create an empty list of packages to populate
missing_packages = []


# check for goccryptfs (for encrypting and decrypting)
if not command_exists("gocryptfs"):
    missing_packages += ["gocryptfs"]


# check for GStreamer (for video wallpapers in Desktop Mode)
if not file_exists(["/usr/lib/gstreamer-1.0/libgstpipewire.so"]):
    missing_packages += ["gst-plugin-pipewire", "gst-libav", "qt-gstreamer"]


# check for KDE Connect (to connect other devices to share notifications, clipboard, files, etc)
if not file_exists(["/usr/lib/kdeconnectd"]):
    # install `kdeconnect` with overwrite_conflicts=True; don't drop sudo until after other installs
    pacman_install(["kdeconnect"], overwrite_conflicts=True, drop_sudo_when_done=False)
    
    # start KDE Connect daemon
    try:
        pid = os.fork() # create independent process for daemon
        if not pid:
            subprocess.run(["/usr/lib/kdeconnectd"], capture_output=True)
            exit()
    except Exception as e:
        error_msg = str(e) + "\nWARNING!: An exception occurred while running `/usr/lib/kdeconnectd`!"
        print(error_msg, ' ', '\n', sys.stderr)
        create_zenity_dialog(error_msg, ZenityDialogType.ERROR)


# check for OpenVPN system integration
if not file_exists(["/usr/lib/NetworkManager/libnm-vpn-plugin-openvpn.so"]):
    missing_packages += ["networkmanager-openvpn"]


# check for video thumbnails
if not file_exists(["/usr/lib/qt/plugins/ffmpegthumbs.so"]):
    missing_packages += ["ffmpegthumbs"]

    # enable thumbnails
    config_path = os.path.expanduser("~") + "/.config/dolphinrc"
    if os.path.isfile(config_path):
        lines = []
        
        try:
            with open(config_path, 'r') as file:
                lines = file.readlines()
        except Exception as e:
            warning_msg = str(e) + "\nWARNING!: An exception occurred while trying to read Dolphin file browser's configuration file!"
            print(warning_msg, ' ', '\n', sys.stderr)
            create_zenity_dialog(warning_msg, ZenityDialogType.ERROR)

        try:
            with open(config_path, 'w') as file:
                for line in lines:
                    if line[:8] == "Plugins=":
                        if not "ffmpegthumbs" in line:
                            if line[-1] == '\n':
                                line = line[:-1]
                            line += ",ffmpegthumbs"
                    file.write(line)
        except Exception as e:
            warning_msg = str(e) + "\nERROR!: An exception occurred while trying to write to Dolphin file browser's configuration file!"
            print(warning_msg, ' ', '\n', sys.stderr)
            create_zenity_dialog(warning_msg, ZenityDialogType.ERROR)

        # todo: handle the case of an entirely missing Dolphin config


# check for webp thumbnails
if not file_exists(["/usr/lib/qt/plugins/imageformats/libqwebp.so"]):
    missing_packages += ["qt5-imageformats"]


# check for libhandy (mobile-friendly widgets for GTK)
if not file_exists(["/usr/lib/libhandy-1.so"]):
    missing_packages += ["libhandy"]


# check for screen
if not command_exists("screen"):
    # install `screen` with overwrite_conflicts=True; don't drop sudo until after other installs
    pacman_install(["screen"], overwrite_conflicts=True, drop_sudo_when_done=False)


# check for firewall
if not command_exists("ufw"):
    # install `ufw` with overwrite_conflicts=True; don't drop sudo until after other installs
    pacman_install(["ufw"], overwrite_conflicts=True, drop_sudo_when_done=False)


# install missing packages; don't drop sudo
if missing_packages != []:
    pacman_install(missing_packages, drop_sudo_when_done=False)


# build_wallpaper_engine_kde_plugin() # todo: fix


# drop sudo privileges to prevent abuse of sudo timeout
drop_sudo()
