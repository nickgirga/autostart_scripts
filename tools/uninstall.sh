#!/usr/bin/env bash
# uninstall.sh
# by Nicholas Girga

# A simple uninstall script for autostart_scripts

# Released under the MIT license with ABSOLUTELY NO WARRANTY


INSTALL_DIR="$HOME/.local/share/autostart_scripts"
AUTOSTART_DIR="$HOME/.config/autostart"


zenity --question --ellipsize --text="Would you like to uninstall autostart_scripts?"
if [ ! "$?" == "0" ];
then
    zenity --info --ellipsize --text="Stopping uninstallation (user request)."
    exit
fi


if [ -d "$INSTALL_DIR" ];
then
    rm -rf "$INSTALL_DIR"
else
    zenity --error --ellipsize --text="autostart_scripts install directory did not appear to exist!"
fi


if [ -f "$AUTOSTART_DIR/Auto Installer.desktop" ];
then
    rm -f "$AUTOSTART_DIR/Auto Installer.desktop"
else
    zenity --error --ellipsize --text="Autostart shortcut did not appear to exist!"
fi


zenity --info --ellipsize --text="autostart_scripts has been removed!"
