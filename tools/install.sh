#!/usr/bin/env bash
# install.sh
# by Nicholas Girga

# A simple install script for autostart_scripts

# Released under the MIT license with ABSOLUTELY NO WARRANTY


REPOSITORY="https://gitlab.com/nickgirga/autostart_scripts.git"
INSTALL_DIR="$HOME/.local/share/autostart_scripts"
AUTOSTART_DIR="$HOME/.config/autostart"


zenity --question --ellipsize --text="Would you like to install autostart_scripts?"
if [ ! "$?" == "0" ];
then
    zenity --info --ellipsize --text="Stopping installation (user request)."
    exit
fi


if [ ! -d "$INSTALL_DIR" ];
then
    mkdir -p "$HOME/.local/share"
    git clone $REPOSITORY "$INSTALL_DIR"
else
    zenity --error --ellipsize --text="autostart_scripts is already installed on this machine!"
    exit 1
fi


if [ ! -f "$AUTOSTART_DIR/Auto Installer.desktop" ];
then
    mkdir -p "$AUTOSTART_DIR"
    ln -s "$INSTALL_DIR/Auto Installer.desktop" "$AUTOSTART_DIR/Auto Installer.desktop"
else
    zenity --error --ellipsize --text="A shortcut already exists in the autostart folder! Install may not be complete!"
    exit 2
fi


zenity --info --ellipsize --text="Finished installing autostart_scripts!"
