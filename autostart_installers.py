#!/usr/bin/env python
# autostart_installers.py
# by Nicholas Girga

# A library of tools for automatically installing software on SteamOS

# Released under the MIT license with ABSOLUTELY NO WARRANTY


# region: Imports


import os, subprocess, sys
from enum import Enum
from typing import List


# endregion: Imports


# region: Global Variables


# Allows the script to run on **unsupported** operating systems
# This is not and will not ever be a fully supported option; it is for debugging
SKIP_OS_CHECK = False


# Set SUDO_ASKPASS so we can easily elevate privileves
host_env = os.environ.copy()
host_env["SUDO_ASKPASS"] = "/usr/bin/ksshaskpass"


# Define the types of Zenity dialogs we can use
# todo: elaborate on create_zenity_dialog(str, ZenityDialogType) and add new ZenityDialogTypes
class ZenityDialogType(Enum):
    INFO = "--info"
    ERROR = "--error"
    QUESTION = "--question"


# endregion: Global Variables


# region: Public Methods


# Creates a dialog for informing the user or gathering yes/no input
# todo: elaborate on this function to allow usage of other zenity dialogs
def create_zenity_dialog(message: str, dialog_type: ZenityDialogType = ZenityDialogType.INFO) -> bool:
    try:
        result = subprocess.run(["zenity", dialog_type.value, "--ellipsize", "--text=" + message], capture_output=True).returncode
        return True if result == 0 else False
    except Exception as e:
        print(str(e) + "\nERROR!: An exception occurred while trying to create a Zenity window!", ' ', '\n', sys.stderr)
        exit("zenity-fail")


# This will use bash's `command -v <query>` feature to determine if a command is available
def command_exists(command: str) -> bool:
    try:
        result = not subprocess.run(["bash", "-c", "command -v " + command], capture_output=True).stdout.decode("UTF-8") == ""
    except Exception as e:
        print(str(e) + "\nERROR!: Bash could not determine if the specified command exists: \"" + command + "\".", ' ', '\n', sys.stderr)
        exit("command-check-fail")
    return result


# This will check to see if a file exists at any specified locations; useful for libraries
# This method is less reliable than command_exists() because it depends on hardcoded paths
def file_exists(potential_paths: List[str]) -> bool:
    for path in potential_paths:
        if os.path.isfile(path):
            return True
    return False


# Installs software to the system using `pacman`; returns False when skipped
def pacman_install(packages: List[str], additional_parameters: List[str] = None, prompt = True, completion_notif = True, needed = True, overwrite_conflicts = False, drop_sudo_when_done = True) -> bool:
    if prompt:
        package_str = ""
        package_count = 0
        for package in packages:
            package_str += ("\t" + package)
            if not package_count >= len(packages) - 1:
                package_str += "\n"
            package_count += 1
        if not create_zenity_dialog("Would you like to install the following package" + ("s" if len(packages) > 1 else "") + "?:\n\n" + package_str, ZenityDialogType.QUESTION):
            return False

    # disable read-only protection if needed
    if not SKIP_OS_CHECK:
        if steamos_readonly_status():
            steamos_readonly_disable()

    # initialize pacman keyring if needed
    if not os.path.isdir("/etc/pacman.d/gnupg"):
        if not pacman_keyring_initialize():
            error_msg = "Pacman keyring could not initialize!"
            print(error_msg, ' ', '\n', sys.stderr)
            create_zenity_dialog(error_msg, ZenityDialogType.ERROR)
            return False

    # construct the installation command
    command = ["sudo", "-A", "pacman", "-Syyu", "--noconfirm"]
    if needed:
        command += ["--needed"]
    command += packages
    if additional_parameters != None:
        command + additional_parameters
    if overwrite_conflicts:
        command += ["--overwrite", "*"]
    
    # create installation subprocess
    try:
        result = subprocess.run(command, capture_output=True, env=host_env)
        if result.returncode != 0:
            error_msg = result.stderr.decode("UTF-8") + "\nERROR!: Package installation failed!"
            print(result.stdout.decode("UTF-8"))
            print(error_msg, ' ', '\n', sys.stderr)
            create_zenity_dialog(error_msg, ZenityDialogType.ERROR)
            return False

    except Exception as e:
        error_msg = str(e) + "\nERROR!: An exception occurred during package installation!"
        print(error_msg, ' ', '\n', sys.stderr)
        create_zenity_dialog(error_msg, ZenityDialogType.ERROR)
        return False

    # enable read-only protection again if needed
    if not SKIP_OS_CHECK:
        if not steamos_readonly_status():
            steamos_readonly_enable()

    # inform user of completion
    if completion_notif:
        completion_msg = "Finished installing package" + ("s" if len(packages) > 1 else "") + "!:\n\n" + package_str
        print(completion_msg)
        create_zenity_dialog(completion_msg)

    # drop sudo to prevent abuse of sudo timeout after install
    if drop_sudo_when_done:
        drop_sudo()

    return True


# Drops sudo privileges to prevent abuse of sudo timeout
# Should not need to be called unless you used "drop_sudo=False" or used private methods
def drop_sudo():
    try:
        subprocess.run(["sudo", "-K"], env=host_env)
    except Exception as e:
        print(str(e) + "\nERROR!: An exception occurred while dropping sudo privileges!", ' ', '\n', sys.stderr)
        exit("drop-sudo")


# endregion: Public Methods


# region: Private Methods


# Returns True if the system is read-only and False if write is enabled
# This is used internally and should not need to be called
def steamos_readonly_status() -> bool:
    try:
        result = subprocess.run(["sudo", "-A", "steamos-readonly", "status"], capture_output=True, env=host_env).stdout.decode("UTF-8") == "enabled\n"
        return result
    except Exception as e:
        print(str(e) + "\nERROR!: An exception occurred while trying to fetch the status of the SteamOS read-only protection!", ' ', '\n', sys.stderr)
        exit("steamos-readonly-status")

    # should not be reached
    return False


# Disables the read-only protection on SteamOS
# This is used internally and should not need to be called
def steamos_readonly_disable():
    try:
        subprocess.run(["sudo", "-A", "steamos-readonly", "disable"], env=host_env)
    except Exception as e:
        print(str(e) + "\nERROR!: An exception occurred while trying to disable SteamOS's read-only protection!", ' ', '\n', sys.stderr)
        exit("steamos-readonly-disable")


# Enables the read-only protection on SteamOS
# This is used internally and should not need to be called
def steamos_readonly_enable():
    try:
        subprocess.run(["sudo", "-A", "steamos-readonly", "enable"], env=host_env)
    except Exception as e:
        print(str(e) + "\nERROR!: An exception occurred while trying to enable SteamOS's read-only protection!", ' ', '\n', sys.stderr)
        exit("steamos-readonly-enable")


# Initializes and populates the keyring for `pacman` to prepare it for installing; returns False on failure
# This is used internally and should not need to be called
def pacman_keyring_initialize() -> bool:
    # initialize
    try:
        result = subprocess.run(["sudo", "-A", "pacman-key", "--init"], env=host_env, capture_output=True)
        if result.returncode != 0:
            error_msg = result.stderr.decode("UTF-8") + "\nERROR!: Keyring initialization failed!"
            print(error_msg, ' ', '\n', sys.stderr)
            create_zenity_dialog(error_msg, ZenityDialogType.ERROR)
            return False
    except Exception as e:
        print(str(e) + "\nERROR!: An exception occured while initializing pacman's keyring!", ' ', '\n', sys.stderr)
        return False
    
    # populate
    try:
        result = subprocess.run(["sudo", "-A", "pacman-key", "--populate", "archlinux"], host_env)
        if result.returncode != 0:
            error_msg = result.stderr.decode("UTF-8") + "\nERROR!: Keyring population failed (archlinux)!"
            print(error_msg, ' ', '\n', sys.stderr)
            create_zenity_dialog(error_msg, ZenityDialogType.ERROR)
            return False
        
        subprocess.run(["sudo", "-A", "pacman-key", "--populate", "holo"])
        if result.returncode != 0:
            error_msg = result.stderr.decode("UTF-8") + "\nERROR!: Keyring population failed (holo)!"
            print(error_msg, ' ', '\n', sys.stderr)
            create_zenity_dialog(error_msg, ZenityDialogType.ERROR)
            return False

    except Exception as e:
        print(str(e) + "\nERROR!: An exception occurred while populating pacman's keyring!", ' ', '\n', sys.stderr)
        return False

    return True


# endregion: Private Methods


# Initialization
if SKIP_OS_CHECK:
    print(  "WARNING!: SKIP_OS_CHECK is enabled! Read-only protection will not be disabled, which may break your packages if you are actually using a read-only system, such as SteamOS.\n" +
            "The SKIP_OS_CHECK varaible is strictly for debugging purposes and not to be used by users (hence the lack of a user-facing interface for it).\n" +
            "If you are not a developer actively working on the autostart_installers library, please set the SKIP_OS_CHECK variable back to False.")

if not command_exists("steamos-readonly") and not SKIP_OS_CHECK:
    print("ERROR!: `steamos-readonly` could not be found on the system! This tool is only intended for SteamOS!", ' ', '\n', sys.stderr)
    exit("no-steamos")
